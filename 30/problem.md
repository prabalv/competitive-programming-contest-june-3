### Area of Rectangle

Rectangular strips of equal width but not necessarily equal height
are cut from a strip of paper and arranged vertically on their base as shown in the figure:

![](images/problem30.png)

Assume that the **__width of each rectangle is 1__**. The heights vary of course. For each arrangement (permutation) **__Pi__** of the rectangular strips, let **__Ai__** be the largest rectangular area possible.


For the arrangement as shown in the figure above, the area of the largest rectangle possible is 9 units2.

Let **__A = max {Ai}__**. What is the value of A given heights of N rectangular strips? (That is the maximum possible rectangular area among all arrangements of the rectangular strips.)


**Input Format**

The first line of input consists of an integer T. Then T lines follow with the input as follows:

N H1 H2 … HN

Here N is the number of rectangular strips, and Hi is the height of the ith rectangle.

**Output Format**

For each test case, print out the integer A where A is defined as above.

**Sample Input:**
```
1
4 1 2 3 4
```

**Sample Output:**
```
6
```
