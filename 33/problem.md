### Finite Continuous Fraction

A finite continuous fraction is an expression in the form:

![](images/problem33.png)

Here **__a0__** is an integer while the ais where i ranges from 1 to n are all positive.

The fraction can also be written in a succinct form thusly:

__[a0; a1, a2, …, an]__

So, given a rational number in the form **__p/q__** where p and q are integers, and both non-zero, find the largest integer in the continued fraction representation. i.e. if p/q is expressed in the form **__[a0, a1, a2, …, an]__**, find the maximum value of all the ai's. Here i ranges from 0 to n.

**Input format**

The first line of input consists of the integer T. This is the number of test cases. Then T lines follow with the format as follows:

p q

Here p and q are non-zero integers.

**Output Format**

For each testcase, output the required integer as outlined above.

**Constraints**

1 <= T <= 1000

-10^9 < p,q < 10^9; p,q !=0

**Sample Input**
```
1
415 93
```

**Sample Output**
```
7
```
