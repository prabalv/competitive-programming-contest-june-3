### Problem Statement 20

You are given a dictionary D of N words. (The length of each word is at least two letters.) You also have a grid of 4 by 4 letters. Find all valid unique words in the grid (with respect to the dictionary D) that can be formed from sequentially adjacent letters in the grid (horizontally, vertically, diagonally; you can go backwards as well.) Output the words in alphabetical order.


For example, if your dictionary consists of the words int, num, float, string, double then the words that can be formed in the following grid:


D O U B

A N M L

T O E F

I N L G


Are double, float, int and num (in alphabetical order.)


**Note:** Don�t pay attention to case. And if the same word is found multiple times in the grid, report it only once.


**Input Format**


The first line of input is an integer T that specifies the number of test cases. 

Then the test cases follow. Each test case consists of 5 lines as input as follows:

N W<sub>0</sub> W<sub>1</sub> � W<sub>N-1</sub>

L<sub>00</sub> L<sub>01</sub> L<sub>02</sub> L<sub>03</sub>

L<sub>10</sub> L<sub>11</sub> L<sub>12</sub> L<sub>13</sub>

L<sub>20</sub> L<sub>21</sub> L<sub>22</sub> L<sub>23</sub>

L<sub>30</sub> L<sub>31</sub> L<sub>32</sub> L<sub>33</sub>

L<sub>40</sub> L<sub>41</sub> L<sub>42</sub> L<sub>43</sub>


Here N is the number of words in the dictionary, the W<sub>i</sub>s are the words in the dictionary, and L<sub>ij</sub> is the letter in the i<sup>th</sup> row and j<sup>th</sup> column in the grid.


(Note that for each test case, you should follow this format!)


**Output Format**


For each test case, output all the valid words found in the grid in alphabetical order, and in lower case:


G<sub>0</sub> G<sub>1</sub> G<sub>2</sub> � G<sub>K</sub> 


(Assuming K words are found in the grid.)


**Constraints**

Let E<sub>i</sub> be the length of the word W<sub>i</sub>. Then 2 <= E<sub>i</sub> <= 16


1<=T<=10000

1<=N<=10000


**Sample Input**
```
1
5 int num float string double
D O U B
A N M L
T O E F
I N L G
```

**Sample Output**
```
double float int num
```